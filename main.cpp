#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/poll.h>

#define MAX_BUFFER_SIZE 512
#define DEVICE_NAME0 "/dev/rpmsg_pru30"
#define DEVICE_NAME1 "/dev/rpmsg_pru31"

char readBuf[MAX_BUFFER_SIZE];

int main(void)
{
   struct pollfd pollfds[2];
   int result = 0;
   int pru_data;
 
   /* Open the rpmsg_pru character device file */
   pollfds[0].fd = open(DEVICE_NAME0, O_RDWR);
   pollfds[1].fd = open(DEVICE_NAME1, O_RDWR);
   /*
   * If the RPMsg channel doesn't exist yet the character device
   * won't either.
   * Make sure the PRU firmware is loaded and that the rpmsg_pru
   * module is inserted.
   */
   if (pollfds[0].fd < 0) {
      printf("Failed to open %s\n", DEVICE_NAME0);
      return -1;
   }
 
   if (pollfds[1].fd < 0) {
      printf("Failed to open %s\n", DEVICE_NAME1);
      return -1;
   }
 
   /* Send 'hello world!' to the PRU through the RPMsg channel */
   result = write(pollfds[0].fd, "hello world_0!", 14);

   result = write(pollfds[1].fd, "hello world_1!", 14);

   /* Poll until we receive a message from the PRU and then print it */
   result = read(pollfds[0].fd, readBuf, MAX_BUFFER_SIZE);
   if (result > 0)
      printf("Message received from PRU_0: %s\n\n", readBuf);

   /* Close the rpmsg_pru character device file */
   close(pollfds[0].fd);

   result = read(pollfds[1].fd,readBuf, MAX_BUFFER_SIZE);
   if(result > 0) 
      printf("Message received from PRU_1: %s\n", readBuf);

   close(pollfds[1].fd);
   return 0;
}
