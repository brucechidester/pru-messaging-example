mkdir /usr/include/pruss
cp am335x_pru_package/pru_sw/app_loader/include/* /usr/include/pruss/
cd am335x_pru_package/pru_sw/app_loader/interface/
CROSS_COMPILE= make
cp ../lib/* /usr/lib/
ldconfig
cd ../../utils/pasm_source
source linuxbuild
cd ..
cp pasm /usr/bin/
cd /usr/include/
ln -s ./pruss/prussdrv.h .
ln -s ./pruss/pruss_intc_mapping.h .
