#! /bin/bash

if [ ! -d "/sys/class/remoteproc/remoteproc0/" ]; then
   echo "remoteproc0 does not exist"
   exit
fi

if [ ! -d "/sys/class/remoteproc/remoteproc1/" ]; then
   echo "remoteproc1 does not exist"
   exit
fi

if [ ! -d "/sys/class/remoteproc/remoteproc2/" ]; then
   echo "remoteproc2 does not exist"
   exit
fi

echo 'stop' > /sys/class/remoteproc/remoteproc1/state
echo 'stop' > /sys/class/remoteproc/remoteproc2/state

cp PRU0_Init.out /lib/firmware/am335x-pru0-fw
cp PRU1_Init.out /lib/firmware/am335x-pru1-fw

echo 'am335x-pru0-fw' > /sys/class/remoteproc/remoteproc1/firmware
echo 'am335x-pru1-fw' > /sys/class/remoteproc/remoteproc2/firmware
echo 'start' > /sys/class/remoteproc/remoteproc1/state
echo 'start' > /sys/class/remoteproc/remoteproc2/state

ls /dev/ | grep pru
