#! /bin/bash

if [ ! -d "/sys/class/remoteproc/remoteproc0/" ]; then
   echo "remoteproc0 does not exist"
   exit
fi

if [ ! -d "/sys/class/remoteproc/remoteproc1/" ]; then
   echo "remoteproc1 does not exist"
   exit
fi

if [ ! -d "/sys/class/remoteproc/remoteproc2/" ]; then
   echo "remoteproc2 does not exist"
   exit
fi

#If changing these variables, make sure the given pin can be muxed to the given pru.  
HEADER=P8_
PIN_NUMBER=45

echo "-Building project"
   cd PRU0
   make clean
   make
   if [ "$?" -ne 0 ]; then
      echo "ERROR: PRU0 did not build!"
      exit
   fi

echo "-Placing the PRU0 firmware"
   cp gen/*.out /lib/firmware/am335x-pru0-fw

   cd ../PRU1
   make clean
   make
   if [ "$?" -ne 0 ]; then
      echo "ERROR: PRU1 did not build!"
      exit
   fi

echo "-Placing the PRU1 firmware"
   cp gen/*.out /lib/firmware/am335x-pru1-fw

echo "-Configuring pinmux"
   config-pin $HEADER$PIN_NUMBER pruout
   config-pin -q $HEADER$PIN_NUMBER

echo "-Rebooting"
      echo "Rebooting pru-core 0"
      echo 'stop' > /sys/class/remoteproc/remoteproc1/state 2>/dev/null
      echo "am335x-pru0-fw" > /sys/class/remoteproc/remoteproc1/firmware
      echo 'start' > /sys/class/remoteproc/remoteproc1/state
      echo "Rebooting pru-core 1"
      echo 'stop' > /sys/class/remoteproc/remoteproc2/state 2>/dev/null
      echo "am335x-pru1-fw" > /sys/class/remoteproc/remoteproc2/firmware
      echo 'start' > /sys/class/remoteproc/remoteproc2/state

echo "-Building Main"
   cd ..
   make clean
   make
   if [ "$?" -ne 0 ]; then
      echo "ERROR: Main did not build!"
      exit
   fi
   echo "Done. up on pin $HEADER$PIN_NUMBER"
   echo "Running application:"
   ./Main

